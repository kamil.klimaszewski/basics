package pl.introduction;

import java.util.Scanner;

public class Ex17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number: ");
        int number = scanner.nextInt();
        System.out.println(fizzBuzz(number));
    }

    public static String fizzBuzz(int number) {
        String result = "";
        if (number % 3 == 0) {
            result = result + "fizz";
        }
        if (number % 5 == 0) {
            result = result + "buzz";
        }
        return result;
    }
}
