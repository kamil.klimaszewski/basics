package pl.introduction;

import java.util.Scanner;

public class Ex7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String knownName = "John Wick";
        System.out.println("Enter you name");
        String name = scanner.nextLine();
        if (knownName.equals(name)) {
            System.out.println("Nice to see you again John");
        } else {
            System.out.printf("Hello %s %n", name);
        }

    }
}
