package pl.introduction;

import java.util.Scanner;

public class Ex23 {
    public static void main(String[] args) {
        System.out.println("Enter text");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println("Beginning position");
        int beginning = scanner.nextInt();
        scanner.nextLine();
        System.out.println("End position");
        int end = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Text:" + text);
        System.out.println("Truncated text:" + text.substring(beginning, end));
    }
}
