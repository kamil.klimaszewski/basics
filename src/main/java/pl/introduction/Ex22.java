package pl.introduction;

import java.util.Scanner;

public class Ex22 {
    public static void main(String[] args) {
        System.out.println("Enter first text");
        Scanner scanner = new Scanner(System.in);
        String text1 = scanner.nextLine();
        System.out.println("Enter second text");
        String text2 = scanner.nextLine();
        System.out.println("The text are equal:" + text1.equals(text2));
    }
}
