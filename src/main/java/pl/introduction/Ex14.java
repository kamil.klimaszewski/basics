package pl.introduction;

import java.util.Scanner;

public class Ex14 {
    public static void main(String[] args) {
        System.out.println("Enter two integers for multiplying");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int range = scanner.nextInt();
        System.out.println("Multiplying number " + number);
        for(int i = 1; i <= range; i++) {
            System.out.printf("%d * %d = %d %n", number, i, number * i);
        }
    }
}
