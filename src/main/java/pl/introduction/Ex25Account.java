package pl.introduction;

//TODO use BigDecimal for money computations
public class Ex25Account {
    private static double interestRate;
    private double amount;

    public Ex25Account(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public static void setInterestRate(double newInterestRate) {
        interestRate = newInterestRate;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public void addMonthlyInterests() {
        amount = amount + amount * interestRate /12;
    }
}
