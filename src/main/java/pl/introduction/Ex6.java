package pl.introduction;

import java.util.Scanner;

public class Ex6 {
    public static void main(String[] args) {
        final String password = "secret";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password");
        String entered = scanner.nextLine();
        System.out.println("Entered password is " + entered);
        if (password.equals(entered)) {
            System.out.println("Hello in secret place!");
        }
    }
}
