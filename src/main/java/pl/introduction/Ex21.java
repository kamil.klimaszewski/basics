package pl.introduction;

import java.util.Scanner;

public class Ex21 {
    public static void main(String[] args) {
        System.out.println("Enter the text to check:");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String reversed = new StringBuilder(text).reverse().toString();
        if (text.equals(reversed)) {
            System.out.println("Palindrome");
        } else {
            System.out.println("No Palindrome");
        }
    }
}
