package pl.introduction;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter integer ");
        int i = scanner.nextInt();
        System.out.println("Your number is " + i);
    }
}
