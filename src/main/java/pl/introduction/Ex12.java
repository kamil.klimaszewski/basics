package pl.introduction;

import java.util.Scanner;

public class Ex12 {
    public static void main(String[] args) {
        System.out.println("Enter integer number");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        printSmallerOddNumbers(number);
    }

    private static void printSmallerOddNumbers(int number) {
        for (int i = 0; i < number; i++) {
            if (isOdd(i)) {
                System.out.println(i);
            }
        }
    }

    private static boolean isOdd(int i) {
        return i % 2 == 1;
    }
}
