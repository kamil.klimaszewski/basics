package pl.introduction;

import java.util.Scanner;

public class Ex10 {
    public static void main(String[] args) {
        System.out.println("How many iterations?");
        Scanner scanner = new Scanner(System.in);
        int max = scanner.nextInt();
        for (int i = 0; i < max; i++) {
            System.out.printf("Iteration number %d %n", i);
        }
    }
}
