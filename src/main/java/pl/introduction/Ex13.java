package pl.introduction;

import java.util.Arrays;

//TODO wrong result presented
public class Ex13 {
    public static void main(String[] args) {
        int[] array = new int[10];
        for(int i = 0; i < array.length; i++) {
            array[i] = randomNumberFromMinus10To10();
        }
        printArray(array);
        System.out.println("Min: " + min(array));
        System.out.println("Max: " + max(array));
    }

    private static int randomNumberFromMinus10To10() {
        return (int)(Math.random() * 21 - 10);
    }

    private static void printArray(int[] array) {
        for(int i: array) {
            System.out.print(i);
            System.out.print(",");
        }
        System.out.println();
    }

    //TODO write on your own
    private static int min(int[] array) {
        return Arrays.stream(array).min().getAsInt();
    }

    //TODO write on your own
    private static int max(int[] array) {
        return Arrays.stream(array).max().getAsInt();
    }
}
