package pl.introduction;

import java.util.Scanner;

public class Ex20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter two numbers:");
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        scanner.nextLine();
        System.out.println("Select operation (+, -, *, /):");
        String operation = scanner.nextLine();
        switch (operation) {
            case ("+"):
                System.out.printf("%f %s %f = %f", a, operation, b, a+b);
                break;
            case ("-"):
                System.out.printf("%f %s %f = %f", a, operation, b, a-b);
                break;
            case ("*"):
                System.out.printf("%f %s %f = %f", a, operation, b, a*b);
                break;
            case ("/"):
                System.out.printf("%f %s %f = %f", a, operation, b, a/b);
                break;
            default:
                System.out.println("Unknown operation:" + operation);
        }


    }
}
