package pl.introduction;

import java.util.Scanner;

//TODO wrong answer
public class Ex8 {
    public static void main(String[] args) {
        System.out.println("Enter temperature");
        Scanner scanner = new Scanner(System.in);
        double temperature = scanner.nextDouble();
        System.out.printf("C to F %f %n", celsiusToFahrenheit(temperature));
        System.out.printf("F to C %f %n", fahrenheitToCelsius(temperature));
        System.out.printf("C to K %f %n", celsiusToKelvin(temperature));
        System.out.printf("K to C %f %n", kelvinToCelsius(temperature));
        System.out.printf("K to F %f %n", kelvinToFahrenheit(temperature));
        System.out.printf("F to K %f %n", fahrenheitToKelvin(temperature));
    }

    private static double celsiusToFahrenheit(double celsius) {
        return 1.8 * celsius + 32;
    }

    private static double fahrenheitToCelsius(double fahrenheit) {
        return (fahrenheit - 32) / 1.8;
    }

    private static double celsiusToKelvin(double celsius) {
        return celsius + 273.15;
    }

    private static double kelvinToCelsius(double kelvin) {
        return kelvin - 273.15;
    }

    private static double kelvinToFahrenheit(double kelvin) {
        return (kelvin - 273.15)*1.8 + 32;
    }

    private static double fahrenheitToKelvin(double fahrenheit) {
        return (fahrenheit - 32) / 1.8 + 273.15;
    }

}
