package pl.introduction;

import java.util.Scanner;

public class Ex11 {
    public static void main(String[] args) {
        String correctPassword = "secret";
        String password;
        System.out.println("Enter your password");
        Scanner scanner = new Scanner(System.in);
        do {
            password = scanner.nextLine();
            System.out.println("Entered password:" + password);
            if (correctPassword.equals(password)) {
                System.out.println("Correct password");
            } else {
                System.out.println("Wrong password, enter password");
            }
        } while (!correctPassword.equals(password));
    }
}
