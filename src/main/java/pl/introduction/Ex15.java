package pl.introduction;

import java.util.Scanner;

//TODO potential issues, range of int
public class Ex15 {
    public static void main(String[] args) {
        System.out.println("Enter number to reverse");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int reversed = 0;
        int temp = number;
        while (temp != 0) {
            reversed *= 10;
            reversed = reversed + temp % 10;
            temp = temp / 10;
        }

        System.out.printf("%d -> %d", number, reversed);
    }
}
