package pl.introduction;

//To many static fields and methods, but it just to simplify the presentation
public class Ex25 {
    private static Ex25Account account1 = new Ex25Account(1000);
    private static Ex25Account account2 = new Ex25Account(2000);

    public static void main(String[] args) {
        Ex25Account.setInterestRate(0.04);
        printState();
        account1.addMonthlyInterests();
        account2.addMonthlyInterests();
        Ex25Account.setInterestRate(0.05);
        printState();
        account1.addMonthlyInterests();
        account2.addMonthlyInterests();
        printState();
    }

    private static void printState() {
        System.out.println("Current interests rate: " + Ex25Account.getInterestRate());
        System.out.println("Account 1 balance: " + account1.getAmount());
        System.out.println("Account 2 balance: " + account2.getAmount());
    }
}
