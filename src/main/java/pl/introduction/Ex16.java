package pl.introduction;

import java.util.Scanner;

//TODO assume that it is total interest rate
public class Ex16 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter price");
        double price = scanner.nextDouble();
        System.out.println("Enter the number of payments");
        double numberOfPayments = scanner.nextInt();
        double interestRate = 0.0;

        if(numberOfPayments >= 6 && numberOfPayments <= 12 ) {
            interestRate = 2.5;
        } else if (numberOfPayments >= 13 && numberOfPayments <= 24) {
            interestRate = 5;
        } else if (numberOfPayments >= 25 && numberOfPayments <= 48) {
            interestRate = 10;
        } else {
            System.out.println("Invalid number of payments");
            System.exit(0);
        }

        double payment = price * (1+interestRate/100) / numberOfPayments;
        System.out.println("Monthly payment: " + payment);

    }
}
