package pl.introduction;

//My format is yyyy-mm-dd
public class Ex24 {
    public static void main(String[] args) {
        Ex24Date date = new Ex24Date(3, 5, 2021);
        System.out.println("Existing date:" + date);
        System.out.println("Day:" + date.getDay());
        System.out.println("Month:" + date.getMonth());
        System.out.println("Year:" + date.getYear());
        int newYear = 2022;
        System.out.println("Change year to:" + newYear);
        date.setYear(newYear);
        System.out.println("Date after change:" + date);
    }
}
